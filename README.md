# Python Programming for the Humanities

## Overview
3-day course 9:00-12:30

### Day 1
- [1a Introduction to Python](1a_Python_and_Jupyter_Notebook.ipynb)
- [1b API requests](1b_API_requests.ipynb) (Brief introduction)

### Day 2
- [2a Organizing and processing data](2a_Organizing_and_processing_data.ipynb) (emphasized data structures, skipped loops)
- 2b Requests exercise (Continuation of 1b)

### Day 3
- [3a Recap](3a_Recap.ipynb) (more in depth on logical operators)
- [3b Pandas](3b_Pandas.ipynb) (main focus of day 3 - ca. 10:15-12:00)
- [3c Network graphs](3c_Network_graphs.ipynb) (Brief demonstration of pyvis)

### Discarded material: 
- [2b Beyond Excel](old_2b_Beyond_excel.ipynb): pandas and xlwings
- [3a Reusing code](old_3a_Reusing_code.ipynb): functions, objects and packages

### Livecode/solutions:
- [1a: Introduction](1a_Python_and_Jupyter_Notebook_livecode.ipynb)
- [2a: Data structures](2a_Organizing_and_processing_data_livecode.ipynb)
- [1b/2b: Requests](2b_Requests_exercise.ipynb)
- [3a: Recap and logic](3a_Recap_livecode.ipynb)

---

## Running the notebooks online
Run the notebooks in Binder:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.app.uib.no%2Fuib-ub%2Fdh-lab%2Fkurs_2021v.git/master)
- no saving
- no xlwings (discarded material, 2b)
- timeout if inactive


## Installing locally
First, download the contents of this repository as a zip or via git.
Extract the files in your user folder.

### Option 1: Anaconda

1. Install Anaconda: [https://docs.anaconda.com/anaconda/install/](https://docs.anaconda.com/anaconda/install/)
2. Install additional packages (cf environment.yml) or create a virtual environment (The introductory notebooks 1a and 2a may work without this step)
- Option a: open the Anaconda Prompt. Create a virtual environment by pasting and running the following command:
	    `conda env create -f https://git.app.uib.no/uib-ub/dh-lab/kurs_2021v/-/raw/master/environment.yml`
- Option b: Create and run an environment by importing environment.yml in the Anaconda Navigator graphical interface.
- Option c: install requirements manually using pip (you will likely only need to install dhlab and openpyxl if you have Anaconda installed. See option 4)


### Option 3:
Set up a virtual environment using requirements.txt

### Option 4
Prerequisite: jupyter notebook
Install packages from Jupyter Notebook by running the following commands in a code cell:
```
!pip install dhlab
!pip install openpyxl
!pip install pandas
!pip install requests
!pip install xlrd
```


## Opening the notebooks locally
If you do not set up an environment (Could work for notebooks 1a and 2a):
Launch Jupyter Notebook from the start menu.

If you have set up a conda environment:
- Option 1
  - Open the anaconda prompt and run the commands:
  - `conda activate dhpython_may2021`
  - `jupyter notebook`
- Option 2
  - Open Anaconda Navigator
  - Go to the environments menu
  - Choose dhpython_may2021 and wait for it to start
  - Click the play-button and select "Open with Jupyter Notebook"



**Final step: Locate the notebooks from within Jupyter Notebook**

