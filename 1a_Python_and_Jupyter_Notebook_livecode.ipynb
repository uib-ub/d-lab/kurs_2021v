{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "65131c39",
   "metadata": {},
   "source": [
    "# 1a: Introduction to Python & Jupyter Notebook"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f63e58f2",
   "metadata": {},
   "source": [
    "Parts of this notebook are based on the following tutorials:\n",
    "*  *Library Carpentry. Introduction to programming with Python. 2017.* <br>https://librarycarpentry.org/lc-python-intro/. © Software Carpentry<br>\n",
    "* *Python Programming for the Humanities* (Folgert Karsdorp et al.): http://www.karsdorp.io/python-course/\n",
    "<br>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6c64fb6",
   "metadata": {},
   "source": [
    "## Contents\n",
    "-  Using Jupyter Notebook\n",
    "-  Variables\n",
    "-  The `print()` function\n",
    "-  Text variables (strings)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "abaa866b",
   "metadata": {},
   "source": [
    "## Jupyter Notebook\n",
    "Jupyter Notebook allows you to both write and execute Python code inside what resebles a text document. Cells in a notebook can contain markup, such as the text you are now reading, or code you can execute by pressing `Crtl+Enter` or `Shift+Enter`. The latter will select the next cell after executing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "3516dc24",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello world!\n"
     ]
    }
   ],
   "source": [
    "print(\"Hello world!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3bc0ecaa",
   "metadata": {},
   "source": [
    "Python has a built-in function called `print` that displays text to the user. In Jupyter Notebook, any value or output in the last line of code that has any output will also be displayed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "0ed78431",
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "This text will appear\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "'This text will also appear'"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\"This text will not appear\"\n",
    "print(\"This text will appear\")\n",
    "\"This text will also appear\"\n",
    "# This is a comment. Comments are ignored by Python."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aa495beb",
   "metadata": {},
   "source": [
    "When you have selected a cell without editing it (e. g. by clicking in the white margin), these keyboard shortcuts will come in handy:\n",
    "- `a`: insert new cell above\n",
    "- `b`: insert new cell below\n",
    "- `m`: make the selected cell a markup cell\n",
    "- `y`:  make the selected cell a code cell\n",
    "- `dd`: delete the selected cell\n",
    "- `z`: undo deleting cell"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7629dd57",
   "metadata": {},
   "source": [
    "<font color = royalblue>\n",
    "    \n",
    "## Numbers\n",
    "    \n",
    "</font>\n",
    "Like any other programming language, Python works as a calculator. Try calculating the number of minutes in seven weeks in the box below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "30614a83",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "70560"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "7 * 7 * 24 * 60"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a9f0c960",
   "metadata": {},
   "source": [
    "## Variables\n",
    "Variables are names for stored values: numbers, text or various programming concepts we will learn more about later. In Python the `=` symbol assigns the value on the right to the name on the left. Equality in the mathematical or logical sense is written `==`. We will get to that in part 2.\n",
    "\n",
    "The variable is created when a value is assigned to it. Feel free to replace the values below with your favorite book:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "f00c0e01",
   "metadata": {},
   "outputs": [],
   "source": [
    "book = \"Brave New World\"\n",
    "author = \"Aldous Huxley\"\n",
    "copies = 1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b49e7854",
   "metadata": {},
   "source": [
    "Variable names:\n",
    "- cannot start with a digit\n",
    "- cannot contain spaces, quotation marks, or other punctuation\n",
    "- *may* contain an underscore, typically used to separate words in long variable names: this_is_a_long_variable_name"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b461fa9d",
   "metadata": {},
   "source": [
    "### Choosing variable names\n",
    "Python doesn’t care what you call variables as long as they obey the rules above:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "8053b41f",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'Brave New World'"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "banana = \"Brave New World\"\n",
    "banana"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d8e81d8",
   "metadata": {},
   "source": [
    "Use meaningful variable names to help other people understand what the program does.\n",
    "The most important “other person” is your future self.\n",
    "\n",
    "As Python is case sensitive, “Banana”  will be different from “banana” . The convention is to use lowercase variable names.\n",
    "\n",
    "You should avoid variable names that already mean something in the Python language. You will know the name is taken if the text turns green. If you for instance write `print = 2`, the new variable will replace the variable that refers to the built-in function print(). If you try printing something afterwards, you will get an error message because `print` has become a number instead of a function."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "79c2e8d8",
   "metadata": {},
   "source": [
    "### Variables must be created before they are used\n",
    "If a variable doesn’t exist yet, or if the name has been misspelled, Python reports an error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "8e1eed8e",
   "metadata": {},
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'genre' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[1;32m<ipython-input-6-51ca146f43ec>\u001b[0m in \u001b[0;36m<module>\u001b[1;34m\u001b[0m\n\u001b[1;32m----> 1\u001b[1;33m \u001b[0mgenre\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[1;31mNameError\u001b[0m: name 'genre' is not defined"
     ]
    }
   ],
   "source": [
    "genre"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dd116b56",
   "metadata": {},
   "source": [
    "An advantage of using an interactive application such as Jupyter Notebook, is that you can continue running your program if an error occurs in one of the cells. Can you edit the cell so that it runs properly?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8025ae2b",
   "metadata": {},
   "source": [
    "### Print variables\n",
    "- The `print`-function prints almost anything in Python\n",
    "- `Call` the function (i.e., tell Python to run it) by using its name.\n",
    "- Provide values to the function (i.e., the things to print) in parentheses, separated by commas.\n",
    "- To add text to the printout, wrap it in quotation marks\n",
    "- The values passed to the function are called `arguments`\n",
    "\n",
    "The print function accepts multiple arguments:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "086c5e41",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "I have 1 copies of Brave New World by Aldous Huxley\n"
     ]
    }
   ],
   "source": [
    "print(\"I have\", copies, \"copies of\", book, 'by', author)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24255a8e",
   "metadata": {},
   "source": [
    "### Change a variable"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ea6052c6",
   "metadata": {},
   "source": [
    "If you assign a new value to a variable, the old value is lost, unless you have stored it as another variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "f869d9eb",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Old copies: 1\n",
      "Total copies: 2\n"
     ]
    }
   ],
   "source": [
    "old_copies = copies\n",
    "copies = 2\n",
    "print(\"Old copies:\", old_copies)\n",
    "print(\"Total copies:\", copies)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "166095ba",
   "metadata": {},
   "source": [
    "In Python, `+=` is shorthand for incrementing a variable (e. g. `copies = copies + 1`). Look at what happens if you execute the cell below more than once using `ctrl+enter`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "2ddc0dc8",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "I have 8 copies of The Spanish Inquisition\n"
     ]
    }
   ],
   "source": [
    "copies += 2\n",
    "print(\"I have\", copies, \"copies of\", book)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e4ce6695",
   "metadata": {},
   "source": [
    "In Jupyter Notebook, you decide wich order the cells are executed. This can cause errors and unexpected results if you loose track of what the variables contain. .\n",
    "<font color = royalblue><b>Try executing the cell below, and then the print statement in the cell above</b></font>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "9189d247",
   "metadata": {},
   "outputs": [],
   "source": [
    "book = \"The Spanish Inquisition\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9788b45e",
   "metadata": {},
   "source": [
    "As a rule of thumb, you should as far as possible execute cells in the order they appear in your notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4e313f42",
   "metadata": {},
   "source": [
    "\n",
    "Now to another confusing quirk of Jupyter Notebook: if you delete a cell or rename a variable, any variables you have created still exist in memory. <font color = royalblue><b>See what happens if you run the cells below, rename the variable in the first cell to something other than greeting and run them both again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "02b0335a",
   "metadata": {},
   "outputs": [],
   "source": [
    "greeting = \"Hello!\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "16da3dbc",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'Hello!'"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "greeting"
   ]
  },
  {
   "attachments": {
    "image.png": {
     "image/png": "iVBORw0KGgoAAAANSUhEUgAAADsAAAAzCAYAAAApdnDeAAADOElEQVRoBe2ZPW+bUBSGEcovCT+B6U7dPMULG1Ijb52YsmWoUg/emNjbfMlbJpY6aaN8OF4ihoilbqzUQ+UkCrGsMN3trS7G9gVcAS64icHSEXA53HOe8x6ubRAopSiKCUUBZZwl7KqqXSpbKrsCq3ax2vjh4QFFMQEF+pSwqyp2qezKKmtZFt6C3dzc/HOeC7cxK9AyP1nEK2GTKJZFpZPEmfhkEa9Yyrqui4jRST3/vl2o0tSPlWD+cOSF4oUmEWRZRsR0O+QWPUwT3LF2oVUIF4egoulo9SlAu2g2TAyiIQIj8fFsGKoBO1RIx9SgmePZc4al6O7WQLyCvoOy1YBh6NiuVfwxBarKiqAjrrxJYHVZBgkBM1hZJh5wrrDU0rHBQJUGOk5AKLiWAWXaVdnBsi7lgcewrHsJcoR1YGosiAa/i2a0rHVrfFtnC8sDz2DlHGHdM2wz5bQWQqICcNDlf7nZA4RutVlh/L2kbcyvP0xhs8naeLwu5aesY0JjQRIsdhGyOQOLwHoKk1kH5QdLO2gwWPUI/TnJpx1aBFbRbfS9BSpvZUFhNVhVN6Bb85vU6ce376QoaWEZKIu6nHuWZTkwoREZMlGxY3bhTjKnA1gG+0oi0FrRO3rixm/TwE5A2fVTWKLluED5mVLbgMqA/UUisFV2cBb3a8KfJyksDzqFJRqO+nQ+rNKMv8vig/tZso3bR6e5jQ9qBYRUoKpb0E17zirNXRPajY9no2mMW5e/1GnteqBsrFh/BPgqpNmPr3Sa2eJ9s4hXKhtfZ3jPg5L4ZeVTKpuyksVqY9Yeb8H+66PUlB30KtwXbuNXkX3KJErYlAV7M+7FUvb+/h5FMeHx8RFFMeHp6QlFMeH5+RlFMWE4HCJgPz6juraGtU8nwfGw31KPL3AuSTjev8Xwdh/HkoTzi1DeCfIRRqMRAvbzyxi2/i04HvZb6nEbl5KEk4MeRr0DnEgSLtuhvBPkI7y8vOD12x2uNyW0r1iuV2hLm7i+S5+3cMq9sjytixDFsVX3etFXmaf16XlRrGKvx7/u7GGvOrteFOvg5468FuXiZnKu8xFf19en9v3w1zT/34fvvfE/sjsWqI0C+0kAAAAASUVORK5CYII="
    }
   },
   "cell_type": "markdown",
   "id": "c97cef83",
   "metadata": {},
   "source": [
    "If you end up cluttering your memory with variable names, you could always wipe the memory by restarting your notebook - `restart the kernel`:\n",
    "![image.png](attachment:image.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6ee1baa2",
   "metadata": {},
   "source": [
    "### Variable types\n",
    "Text values in Python are called `strings` - they are strings of characters. Numbers are usually `integers` (whole numbers) or `floats` (numbers with decimals). Python picks variable type based on the value assigned to the variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "f98d55f3",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_integer = 1\n",
    "my_float = 1.0\n",
    "my_string = \"1\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b7548f3b",
   "metadata": {},
   "source": [
    "Adding strings together is called `concatenation`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "9a941704",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'1 is a number'"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "my_string + \" is a number\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81ca4134",
   "metadata": {},
   "source": [
    "If you add an integer and a float, the result will be an integer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "6c2838a4",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2.0"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "my_integer + my_float"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e65dbd3d",
   "metadata": {},
   "source": [
    "Addition and concatenation are different things. You will therefore get an error if you try adding a string to a number. You can fix this by converting one of the variables to the correct type using `str()`, `int()` or `float()`, where the variable you want to convert is passed as an argument inside the parentheses. <font color = royalblue><b>Try to make the cell below output a concatenated string or the sum of the two numbers.</b></font>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "25b75d69",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'11'"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "str(my_integer) + my_string"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1091e968",
   "metadata": {},
   "source": [
    "When unsure about the type of a variable, use the function `type()`. <font color = royalblue><b>Try replacing the number with different variables:</b></font>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "7f0ac920",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "str"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "type(\"Hello\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9f0c7c34",
   "metadata": {},
   "source": [
    "## Strings"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "799e22d9",
   "metadata": {},
   "source": [
    "Many disciplines within the humanities work on texts. Quite naturally programming for the humanities will focus on manipulating strings.\n",
    "\n",
    "Each character in a string has an `index` - its position in the string of characters, starting from 0. Putting the index inside square brackets at the end of a string or a string variable returns the character as a string. <font color = royalblue><b>Try replacing the string below with the book variable, or try it with a variable you define yourself. You can try changing the index number as well.</b></font>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "07b27a7f",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'t'"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\"Put a another string or a string variable here\"[2]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9215eeea",
   "metadata": {},
   "source": [
    "If you chose a number higher than the length of your string, you would get an error message. You can find the length of a string using the function `len()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "ccdb325d",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "12"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "my_string = \"I'm a string\"\n",
    "len(my_string)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a98c3b20",
   "metadata": {},
   "source": [
    "Use negative numbers as indices to get characters starting from the end of the string, e. g. -1 to get the last character, -2 to get the before it and so on:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "a56f939e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "n\n",
      "g\n"
     ]
    }
   ],
   "source": [
    "print(my_string[-2])\n",
    "print(my_string[-1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b38bb5f6",
   "metadata": {},
   "source": [
    "It is important to remember that the index starts at 0, which means the length of the string is higher than the index of the last character. This is a common error in programming:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "a9d52e61",
   "metadata": {},
   "outputs": [
    {
     "ename": "IndexError",
     "evalue": "string index out of range",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mIndexError\u001b[0m                                Traceback (most recent call last)",
      "\u001b[1;32m<ipython-input-25-06b2019ae848>\u001b[0m in \u001b[0;36m<module>\u001b[1;34m\u001b[0m\n\u001b[1;32m----> 1\u001b[1;33m \u001b[0mmy_string\u001b[0m\u001b[1;33m[\u001b[0m\u001b[0mlen\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mmy_string\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m]\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[1;31mIndexError\u001b[0m: string index out of range"
     ]
    }
   ],
   "source": [
    "my_string[len(my_string)]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d4d95a2a",
   "metadata": {},
   "source": [
    "### The escape character\n",
    "Backslash (`\\`) has a special meaning in strings. You can use it to have quotation marks without ending the string:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "id": "833aba9d",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'Put \"double quotes\" inside a string '"
      ]
     },
     "execution_count": 26,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\"Put \\\"double quotes\\\" inside a string \""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "id": "1171ce71",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\"Avoid single quote strings if you don't like to use the escape character\""
      ]
     },
     "execution_count": 27,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "'Avoid single quote strings if you don\\'t like to use the escape character'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "080d5456",
   "metadata": {},
   "source": [
    "The escape character has other meanings if followed by specific characters, the most common one being `\\n`, which is a new line if you are reading or writing to a file."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aae7b2c4",
   "metadata": {},
   "source": [
    "### String methods\n",
    "`Methods` are a type of function that can be applied to an `object` such as a string by connecting it with a `.` instead of passing the object (in this case the string) as an argument.\n",
    "\n",
    "There are a number of methods specific to strings that are useful when working with text:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "id": "4c3d4884",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "   python \n",
      "   PYTHON \n",
      "Python\n"
     ]
    }
   ],
   "source": [
    "python = \"   Python \"\n",
    "print(python.lower()) # returns a new string in lowercase\n",
    "print(python.upper()) # returns a new sting in uppercase\n",
    "print(python.strip()) # removes whitespace around the string"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d323e2bb",
   "metadata": {},
   "source": [
    "Replace a part of a string with `replace()`. You shoud look into [regular expressions](https://lingkurs.h.uib.no/webroot/index.php?page=python/python-re&lang=en&course=ling123) if you are interested in more advanced ways of replacing parts of strings or matching patterns in a string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "id": "103a9cb0",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'Python'"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\"Pytheen\".replace(\"ee\", \"o\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ad6956b9",
   "metadata": {},
   "source": [
    "By providing arguments in the `.strip()` method, you can remove specific characters. <font color = royalblue><b>Try removing the hyphen and the whitespace around the string below:</b></font>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "id": "6064f478",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'remove hyphen,  period and spaces around the string'"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "text = \"- Remove hyphen,  period and spaces around the string. \"\n",
    "stripped_text = text.strip(\"- .\").upper().lower() # demonstrated that you can apply a new method, such as lower() to the result\n",
    "\n",
    "stripped_text"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "id": "a10d3c1a",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "51"
      ]
     },
     "execution_count": 31,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "len(stripped_text)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "id": "78add665",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "55"
      ]
     },
     "execution_count": 32,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "len(text)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63199bbf",
   "metadata": {},
   "source": [
    "--------"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb662367",
   "metadata": {},
   "source": [
    "## Recap\n",
    "\n",
    "-  Values are assigned to variables with the `=` operator. \n",
    "-  The `print()` function displays values or variables passed as comma separated `arguments` inside the\n",
    "parentheses\n",
    "-  Variables persist between cells in Jupyter Notebook\n",
    "-  `restart kernel`\n",
    "-  Text variables are called `strings`\n",
    "-  Use the built-in function `len()` to find the length of a string.\n",
    "-  Useful string `methods`:\n",
    "    - `.lower()`\n",
    "    - `.upper()`\n",
    "    - `.strip()`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ef8b5e17",
   "metadata": {},
   "source": [
    "--------"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d4d1b281",
   "metadata": {},
   "source": [
    "<a rel=\"license\" href=\"http://creativecommons.org/licenses/by/4.0/\"><img alt=\"Creative Commons License\" style=\"border-width:0\" src=\"https://i.creativecommons.org/l/by/4.0/88x31.png\" /></a><br />This work is licensed under a <a rel=\"license\" href=\"http://creativecommons.org/licenses/by/4.0/\">Creative Commons Attribution 4.0 International License</a>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "31e94c3f",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
