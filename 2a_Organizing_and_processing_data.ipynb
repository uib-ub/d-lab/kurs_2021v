{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "47cfeacf",
   "metadata": {},
   "outputs": [],
   "source": [
    "import requests"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7d230c45",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# 2a: Organizing and processing data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a87934cb",
   "metadata": {},
   "source": [
    "Parts of this notebook are based on *Python tutorial for the Humanities* (Folgert Karsdorp et al.): http://www.karsdorp.io/python-course/"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ffc6b5b3",
   "metadata": {},
   "source": [
    "In part 1, we looked at how to store information in variables and work with strings. This notebook introduces concepts that make programming more efficient than manually reading or editing data in text files or spreadsheets.\n",
    "\n",
    "First we will look at the four basic `data structures` in python, i. e. containers that store more than one value. Then we will get to what computing is all about: creating algorithms that instruct the computer what to do with our data. We will look at how `loops` allow us to execute code multiple times. Finally, you wil get an opportunity to use what you have learned to count words in a file."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aae9d4b0",
   "metadata": {},
   "source": [
    "## Recap\n",
    "The line of code below loads the book \"Alice in Wonderland\" directly into a variable in jupyter notebook.\n",
    "1. How many characters does the book contain (including spaces)?\n",
    "2. What is the 50th character in the book?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "09e8254c",
   "metadata": {},
   "outputs": [],
   "source": [
    "alice_text = requests.get(\"https://www.gutenberg.org/files/11/11-0.txt\").content.decode()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b7aab86c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "500a6e21",
   "metadata": {},
   "source": [
    "## Contents\n",
    "- Data structures\n",
    "    - Lists and tuples\n",
    "    - Dictionaries and sets\n",
    "-  Logic\n",
    "    - if, elif, else\n",
    "    - and, or, not\n",
    "-  For loops\n",
    "-  Final challenge: counting words in a file"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9eafab1c",
   "metadata": {},
   "source": [
    "## Data structures"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e988382d",
   "metadata": {},
   "source": [
    "### Lists\n",
    "- elements are accessed using their position in the index, like the characters in strings\n",
    "- are contained within square brackets `[...]`\n",
    "- contain values separated by commas.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f7ca3cf3",
   "metadata": {},
   "source": [
    "Consider the sentence below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e8ef314e",
   "metadata": {},
   "outputs": [],
   "source": [
    "sentence = \"Python's name is derived from the television series Monty Python's Flying Circus.\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20f41c65",
   "metadata": {},
   "source": [
    "Words are made up of characters, and so are string objects in Python. When representing a sentence as a variable however, it makes more sense to make each word an element of a list.\n",
    "\n",
    "One way of doing that is to split the string on whitespaces using the string method `.split()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "813d6c63",
   "metadata": {},
   "outputs": [],
   "source": [
    "words = sentence.split()\n",
    "print(words)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a7beab67",
   "metadata": {},
   "source": [
    "Lists are in many ways similar to strings. You can:\n",
    "- find their length by passing them as an argument in the `len()` function\n",
    "- join two lists together using `+`. \n",
    "\n",
    "Lists also have an index you can use to fetch the elements:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f8c9df96",
   "metadata": {},
   "outputs": [],
   "source": [
    "words[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bc2b611a",
   "metadata": {},
   "source": [
    "Another useful feature strings and lists have in common is *slicing* - retrieving elements within a certain range:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bfe951f2",
   "metadata": {},
   "outputs": [],
   "source": [
    "words[1:4]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6095c024",
   "metadata": {},
   "source": [
    "#### Modifying lists\n",
    "Strings are `immutable`, meaning the string object itself cannot be changed. When you modify a string, you actually create a new string. Lists on the other hand, can be edited in various ways. We can add new items to a list. For that you use the method append. Let's see how it works. Say we want to keep a list of books. We start with an empty list and we will add some books to it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a0ca0f42",
   "metadata": {},
   "outputs": [],
   "source": [
    "books = []\n",
    "books.append(\"The Hunger games\")\n",
    "books.append(\"A Clockwork Orange\")\n",
    "books"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b094b9a1",
   "metadata": {},
   "source": [
    "Now, if we want to replace one of the books, we can change it as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f7a0690f",
   "metadata": {},
   "outputs": [],
   "source": [
    "books[0] = \"Pride and Prejudice\"\n",
    "books"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "af6e6a5d",
   "metadata": {},
   "source": [
    "We just changed one element in a list. Note that if you do the same thing for a string, you will get an error, because the string is immutable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "36908109",
   "metadata": {},
   "outputs": [],
   "source": [
    "name = \"Pythen\"\n",
    "name[4] = \"o\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4cc72caf",
   "metadata": {},
   "source": [
    "Just as with strings, we can join two lists together using `+`. Here is an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c3c59a0d",
   "metadata": {},
   "outputs": [],
   "source": [
    "books = [\"The Hunger games\", \"A Clockwork Orange\"]\n",
    "\n",
    "other_books = [\"Pride and Prejudice\", \"Water for Elephants\"]\n",
    "\n",
    "all_books = books + other_books\n",
    "print(all_books)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "583b3326",
   "metadata": {},
   "source": [
    "If you rather want to concatenate all the strings in a list, you can use the string method join() on the string you want between the elements, in this case a comma:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b2170170",
   "metadata": {},
   "outputs": [],
   "source": [
    "\", \".join(all_books)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d316eb02",
   "metadata": {},
   "source": [
    "### Tuples\n",
    "- Similar to lists: Elements accessed using the index\n",
    "- Immutable like strings\n",
    "- Enclosed in parentheses\n",
    "\n",
    "Tuples are a lightweight alternative to lists. Since they are immutable, they are useful if you want a permanent sequence, preventing you from accidentally changing the content."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "042b6260",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_tuple = (1, 2, 3, 4)\n",
    "my_tuple[2]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fe3615a4",
   "metadata": {},
   "source": [
    "You can convert between lists and tuples using `list()` and `tuple()`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "edabf96b",
   "metadata": {},
   "source": [
    "--------"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9865de90",
   "metadata": {},
   "source": [
    "<font color = royalblue>\n",
    "    \n",
    "### Exercise 1\n",
    "    \n",
    "</font>\n",
    "Do you remember how to fetch the last character in a string? Try using the index in the same way to get the word \"Circus\" from our word list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c5447203",
   "metadata": {},
   "outputs": [],
   "source": [
    "last_word = # your code here\n",
    "print(last_word)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8cc3286c",
   "metadata": {},
   "source": [
    "Now, try getting a slice of the last three elements of the list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9e62f88f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# insert your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a79d7fa",
   "metadata": {},
   "source": [
    "Try adding and replacing elements in one of our list of books. Then convert the list to a tuple."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c4d5c1d1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# insert your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b10a46a1",
   "metadata": {},
   "source": [
    "-----------"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7e5b0012",
   "metadata": {},
   "source": [
    "### Dictionaries\n",
    "- Access elements using a key value (text) instead of an index position (number)\n",
    "- Keys are usually strings\n",
    "- Keys must be immutable - tuples are allowed, lists are not"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c18255c",
   "metadata": {},
   "source": [
    "Our little book collection is starting to look good and we can perform all kinds of manipulations on it. Now, imagine that our list is large and we would like to look up the score we gave to a particular book. How are we going to find that book? For this purpose Python provides another more appropriate data structure, named `dictionary`. A `dictionary` consists of entries, or keys, that hold a value. Let's define one:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "186e317a",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_dict = {\"book\": \"physical objects consisting of a number of pages bound together\",\n",
    "           \"sword\": \"a cutting or thrusting weapon that has a long metal blade\",\n",
    "           \"pie\": \"dish baked in pastry-lined pan often with a pastry top\"}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5c310290",
   "metadata": {},
   "source": [
    "Take a close look at the new syntax. Notice the curly brackets and the colons. Keys are located at the left side of the colon; values at the right side. To look up the value of a given key, we 'index' the dictionary using that key:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "00d588cf",
   "metadata": {},
   "outputs": [],
   "source": [
    "description = my_dict[\"sword\"]\n",
    "print(description)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "972a16a8",
   "metadata": {},
   "source": [
    "We say 'index', because we use the same syntax with square brackets when indexing lists or strings. The differences is that we don't use a position number to index a dictionary, but a key. Like lists, dictionaries are mutable which means we can add and remove entries from it. Let's define an empty dictionary and add some books to it. The titles will be our keys and the scores their values. Watch the syntax to add a new entry:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "07601567",
   "metadata": {},
   "outputs": [],
   "source": [
    "book_dict = {}\n",
    "book_dict[\"Pride and Prejudice\"] = 8\n",
    "book_dict[\"A Clockwork Orange\"] = 9"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d141c12f",
   "metadata": {},
   "source": [
    "In a way this is similar to what we have seen before when we altered our book `list`. There we indexed the list using a integer to access a particular book. Here we directly use the title of the book. Can you imagine why this is so useful?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5b0552d0",
   "metadata": {},
   "source": [
    "#### keys(), values()\n",
    "To retrieve a list of all the books we have in our collection, we can ask the dictionary to return its keys as a list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "020fbbf3",
   "metadata": {},
   "outputs": [],
   "source": [
    "book_dict.keys()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "84082393",
   "metadata": {},
   "source": [
    "Similarly we can ask for the values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ff08f2f7",
   "metadata": {},
   "outputs": [],
   "source": [
    "book_dict.values()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a56f33bb",
   "metadata": {},
   "source": [
    "#### get()\n",
    "Trying to access the score of a book that is not in your collection raises a `KeyError`, which basically means \"the key you asked is not in the dictionary\":"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c0446f97",
   "metadata": {},
   "outputs": [],
   "source": [
    "book_dict[\"Endemic Parrots of Norway\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "731118e0",
   "metadata": {},
   "source": [
    "One solution to this problem is to use `get()`, which returns a default value if the key is not in the dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2e82750c",
   "metadata": {},
   "outputs": [],
   "source": [
    "book_dict.get(\"Endemic Parrots of Norway\", 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "351d926a",
   "metadata": {},
   "source": [
    "----------"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b1f5a8ae",
   "metadata": {},
   "source": [
    "<font color = royalblue>\n",
    "    \n",
    "### Exercise 2\n",
    "    \n",
    "</font>\n",
    "Update book dictionary with your own books. Try printing out the score you gave for one of the books."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b4226ef7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# insert your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "799aff09",
   "metadata": {},
   "source": [
    "----------"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "82f73fc0",
   "metadata": {},
   "source": [
    "### Sets\n",
    "- Unordered elements\n",
    "- No duplicates\n",
    "- Elements must be immutable\n",
    "- Use curly brackets like dictionaries: `{...}`\n",
    "- Since `{}` is taken, an empty set is created with `set()`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1512be33",
   "metadata": {},
   "source": [
    "If you want to check if a container contains an element or not, sets and dictionaries are much faster than lists and tuples. If you don't need to access the elements with a key, you can use a set instead of a dictionary. Elements are added using the method `.add()`, and you can convert other containers to sets by passing them as an argument in `set()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "242d11c9",
   "metadata": {},
   "outputs": [],
   "source": [
    "book_set = set(books)\n",
    "book_set"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e6f4ba16",
   "metadata": {},
   "source": [
    "### Nested data structures"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2555f334",
   "metadata": {},
   "source": [
    "#### Nested lists\n",
    "Up to this point, our lists only have consisted of strings. However, a list can contain all kinds of data types, such as integers and even lists! Do you understand what is happening in the following example? <font color = \"royalblue\"><b>See what happends if you change the indices.</b></font>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "583c4516",
   "metadata": {},
   "outputs": [],
   "source": [
    "nested_list = [[1, 2, 3, 4], [5, [\"6a\", \"6b\"], 7, 8]]\n",
    "print(nested_list[0])\n",
    "print(nested_list[0][1])\n",
    "print(nested_list[1][0])\n",
    "print(nested_list[1][1])\n",
    "print(nested_list[1][1][0])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c710e113",
   "metadata": {},
   "source": [
    "You can think of a list of lists as a table. The lists inside the list give it a second dimension:\n",
    "\n",
    "| First number | Second number | sum |\n",
    "| --- | --- | --- |\n",
    "| 2 | 2 | 4 |\n",
    "| 4 | 4 | 8 |\n",
    "\n",
    "As a list of lists, you could represent rows as lists inside a list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b59725ea",
   "metadata": {},
   "outputs": [],
   "source": [
    "table = [[\"First number\", \"Second number\", \"sum\"],[2, 2, 4], [4, 4, 8]]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ce87c32",
   "metadata": {},
   "source": [
    "#### Combining different data structures\n",
    "Dictionaries may also be nested - the value associated with a key can itself be a dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b49463e2",
   "metadata": {},
   "outputs": [],
   "source": [
    "nested_dict = {\"A Clockwork Orange\": {\"Author\": \"Anthony Burgess\", \"Score\": 8}}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bca71942",
   "metadata": {},
   "source": [
    "You may also combine lists, sets, tuples and dictionaries in different ways. Just remember that dictionary keys and elements of sets must be immutable. Here is an example of a list of dictionaries:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dbe0ea93",
   "metadata": {},
   "outputs": [],
   "source": [
    "book_list = [{\"Title\":\"A Clockwork Orange\", \"Author\": \"Anthony Burgess\"}, \n",
    "             {\"Title\": \"The Hunger Games\", \"Author\": \"Suzanne Collins\"}]\n",
    "\n",
    "book_list[1][\"Title\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f3bacc28",
   "metadata": {},
   "source": [
    "--------"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b9e14bb2",
   "metadata": {},
   "source": [
    "### Recap\n",
    "To finish this section, here is an overview of the new concepts and functions you have learnt. Go through them and make sure you understand them all.\n",
    "\n",
    "-  Data structures\n",
    "    -  *list*\n",
    "        -  contain elements in a particular order\n",
    "        -  can be edited, while strings are *immutable*\n",
    "        -  `.split()`\n",
    "        -  `.append()`\n",
    "        -  combine lists using `+`\n",
    "        -  concatenate list elements `using join()`\n",
    "    - *tuple*: like an immutable (permanent) list\n",
    "\n",
    "    - *dictionary*\n",
    "        -  keys instead of indices\n",
    "        -  assigning values to keys\n",
    "        -  `.keys()`\n",
    "        -  `.values()`\n",
    "        -  `.get()`\n",
    "    - *set*: unordered unique elements (no duplicates)\n",
    "- Containers can be `nested` and contain other data structures\n",
    "- Dictionary keys and set elements may be immutable data structures - e. g. tuples.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d34458a5",
   "metadata": {},
   "source": [
    "--------"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6485d6f4",
   "metadata": {},
   "source": [
    "<font color = royalblue>\n",
    "    \n",
    "### Exercise 3\n",
    "    \n",
    "</font>\n",
    "Consider this table of how frequently the words \"summer\", \"temperatures\" and \"hot\" occur next to each other in a collection of texts (rows representing the first word in a pair of words, columns representing the second word):\n",
    "\n",
    "|  | summer | temperatures | hot |\n",
    "| --- | --- | --- | --- |\n",
    "| summer |  | 767 | 9 |\n",
    "| temperatures |  |  | 6 |\n",
    "| hot | 1034 | 231 | 52 |\n",
    "\n",
    "How would you represent this table using nested data structures in Python? Try finding a solution in the box below, and put the data in your \"table\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "45ae7e94",
   "metadata": {},
   "outputs": [],
   "source": [
    "# your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0fcd68fa",
   "metadata": {},
   "source": [
    "--------"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf411bcc",
   "metadata": {},
   "source": [
    "## Processing data\n",
    "A lot of programming has to do with executing a certain piece of code if a particular condition holds. In part 1 we mentioned that `==` represents equality in Python. Here we give a brief overview of this and a few other logical operators. Can you figure our what all of the conditions do?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b1a64083",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"2 < 5 =\", 2 < 5)\n",
    "print(\"3 > 7 =\", 3 >= 7)\n",
    "print(\"3 == 4 =\", 3 == 4)\n",
    "print(\"school == homework =\", \"school\" == \"homework\")\n",
    "print(\"Python != Java =\", \"Python\" != \"Java\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "451fc429",
   "metadata": {},
   "source": [
    "### if, elif, else\n",
    "The dictionary is a much better data structure for our book collections. However, even with dictionaries we might forget which books we added to the collection. As we have seen, trying to get the score of a book that is not in our dictionary results in an error message. Let's write a little program that prints \"X is in the collection\" if a particular book is in the collection and \"X is NOT in the collection\" if it is not."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "079791d3",
   "metadata": {},
   "outputs": [],
   "source": [
    "book = \"A Clockwork Orange\"\n",
    "if book in book_dict:\n",
    "    print(book + \" is in the collection:\")\n",
    "    print(\"Score:\", book_dict[book])\n",
    "else:\n",
    "    print(book + \" is NOT in the collection\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f49955f9",
   "metadata": {},
   "source": [
    "A lot of new syntax here. Let's go through it step by step. First we ask if the value we assigned to `book` is in our collection. The part after `if` evaluates to either `True` or to `False`. Let's type that in:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "da3c0a82",
   "metadata": {},
   "outputs": [],
   "source": [
    "\"Endemic Parrots of Norway\" in book_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16881ba7",
   "metadata": {},
   "source": [
    "Because our book is not in the collection, Python returns `False`. Let's do the same thing for a book that we know is in the collection:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ffe6cd45",
   "metadata": {},
   "outputs": [],
   "source": [
    "\"A Clockwork Orange\" in book_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "43e1e1a1",
   "metadata": {},
   "source": [
    "Indeed, it is in the collection. Back to our `if` statement. If the expression after `if` evaluates to `True`, our program will go on to the next line and print `book + \" is in the collection\"`. Let's try that as well:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "432cc8fe",
   "metadata": {},
   "outputs": [],
   "source": [
    "if \"A Clockwork Orange\" in book_dict:\n",
    "    print(\"Found it!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "73468255",
   "metadata": {},
   "outputs": [],
   "source": [
    "absurd_book = \"Endemic Parrots of Norway\"\n",
    "if absurd_book in book_dict:\n",
    "    print(\"Found it!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "010fb275",
   "metadata": {},
   "source": [
    "Notice that the print statement in the last code block is not executed. That is because the value we assigned to `book` is not in our collection and thus the part after `if` did not evaluate to `True`. In our little program above we used another statement besides `if`, namely `else`. It shouldn't be too hard to figure out what's going on here. The part after `else` will be executed if the `if` statement evaluated to `False`. In English: if the book is not in the collection, print that it is not."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e7e4d9ce",
   "metadata": {},
   "source": [
    "#### Indentation!\n",
    "Before we continue, we must first explain to you that the layout of our code is not optional. Unlike in other languages, Python does not make use of curly braces to mark the start and end of expressions. The only delimiter is a colon (`:`) and the indentation of the code. This indentation must be used consistently throughout your code. The convention is to use 4 spaces as indentation. This means that after you have used a colon (such as in our `if` statement) the next line should be indented by four spaces more than the previous line.\n",
    "\n",
    "Sometimes we have various conditions that should all evaluate to something different. For that Python provides the `elif` statement. We use it similar to `if` and `else`. Note however that you can only use `elif` after an `if` statement! Above we asked whether a book was in the collection. We can do the same thing for parts of strings or for items in a list. For example we could test whether the letter *a* is in the word *banana*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6f509d65",
   "metadata": {},
   "outputs": [],
   "source": [
    "\"a\" in \"banana\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6622ea7b",
   "metadata": {},
   "source": [
    "Likewise the following evaluates to `False`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4b112471",
   "metadata": {},
   "outputs": [],
   "source": [
    "\"z\" in \"banana\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "de4ed41a",
   "metadata": {},
   "source": [
    "Let's use this in an `if-elif-else` combination:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "438543e4",
   "metadata": {},
   "outputs": [],
   "source": [
    "word = \"rocket science\"\n",
    "if \"a\" in word:\n",
    "    print(word + \" contains the letter a\")\n",
    "elif \"s\" in word:\n",
    "    print(word + \" contains the letter s\")\n",
    "else:\n",
    "    print(\"What a weird word!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fad92b4f",
   "metadata": {},
   "source": [
    "### and, or, not\n",
    "Up to this point, our conditions have consisted of single expresssions. However, quite often we would like to test for multiple conditions and then execute a particular piece of code. Python provides a number of ways to do that. The first is with the `and` statement. `and` allows us to juxtapose two expressions that need to be true in order to make the entire expression evaluate to `True`. Let's see how that works:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8747818c",
   "metadata": {},
   "outputs": [],
   "source": [
    "word = \"banana\"\n",
    "if \"a\" in word and \"b\" in word:\n",
    "    print(\"Both a and b are in \" + word)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8efec88c",
   "metadata": {},
   "source": [
    "If one of the expressions evaluates to False, nothing will be printed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "66883895",
   "metadata": {},
   "outputs": [],
   "source": [
    "if \"a\" in word and \"z\" in word:\n",
    "    print(\"Both a and z are in \" + word)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1106c743",
   "metadata": {},
   "source": [
    "-------"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b2c63c7c",
   "metadata": {},
   "source": [
    "<font color = royalblue>\n",
    "    \n",
    "### Exercise 4\n",
    "    \n",
    "</font>\n",
    "Replace `and` with `or` in the `if` statement below. What happens? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "125e0920",
   "metadata": {},
   "outputs": [],
   "source": [
    "word = \"banana\"\n",
    "if \"a\" in word and \"z\" in word:\n",
    "    print(\"Both a and b are in \" + word)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0f84ca76",
   "metadata": {},
   "source": [
    "In the code block below, can you add an `else` statement that prints that none of the letters were found?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a625ff90",
   "metadata": {},
   "outputs": [],
   "source": [
    "if \"a\" in word and \"z\" in word:\n",
    "    print(\"Both a and z are in \" + word)\n",
    "# insert your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a814d745",
   "metadata": {},
   "source": [
    "----------"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "917b7b7e",
   "metadata": {},
   "source": [
    "Finally we can use `not` to test for conditions that are not true."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a9e54e6a",
   "metadata": {},
   "outputs": [],
   "source": [
    "if \"z\" not in word:\n",
    "    print(\"z is not in \" + word)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bbb8169c",
   "metadata": {},
   "source": [
    "Objects, such as strings or integers of lists are `True` because they exist. Empty strings, lists, dictionaries etc on the other hand are `False` because in a way they do *not* exist. We can use this principle to, for example, only execute a piece of code if a certain list contains any values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c54f0d22",
   "metadata": {},
   "outputs": [],
   "source": [
    "numbers = [1, 2, 3, 4]\n",
    "if numbers:\n",
    "    print(\"I found some numbers!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "64d26e17",
   "metadata": {},
   "source": [
    "Now if our list were empty, Python wouldn't print anything:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "53b27427",
   "metadata": {},
   "outputs": [],
   "source": [
    "numbers = []\n",
    "if numbers:\n",
    "    print(\"I found some numbers!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eff4c081",
   "metadata": {},
   "source": [
    "--------"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "34e588b3",
   "metadata": {},
   "source": [
    "<font color = royalblue>\n",
    "    \n",
    "### Exercise 5\n",
    "    \n",
    "</font>\n",
    "Can you write code that prints \"This is an empty list\" if the provided list does not contain any values?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e614ed3d",
   "metadata": {},
   "outputs": [],
   "source": [
    "numbers = []\n",
    "# insert your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce1a1355",
   "metadata": {},
   "source": [
    "Can you do the same thing, but this time using the function `len()`?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "336a55d9",
   "metadata": {},
   "outputs": [],
   "source": [
    "numbers = []\n",
    "# insert your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "db349d79",
   "metadata": {},
   "source": [
    "-----------"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "619757ef",
   "metadata": {},
   "source": [
    "### Recap\n",
    "To finish this section, here is an overview of the new functions, statements and concepts we have learnt. Go through them and make sure you understand what their purpose is and how they are used.\n",
    "-  indentation\n",
    "-  conditionals\n",
    "    -  `if`\n",
    "    -  `elif`\n",
    "    -  `else`\n",
    "-  `True`\n",
    "-  `False`\n",
    "-  empty objects are false\n",
    "-  `not`\n",
    "-  `in`\n",
    "-  `and`\n",
    "-  `or`\n",
    "-  multiple conditions\n",
    "-  `==`\n",
    "-  `<`\n",
    "-  `>`\n",
    "-  `!=`\n",
    "-  `KeyError`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cd9f455b",
   "metadata": {},
   "source": [
    "---------"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d546c41",
   "metadata": {},
   "source": [
    "## For loops\n",
    "Programming is most useful if we can perform a certain action on a range of different elements. For example, given a list of words, we would like to know the length of all words, not just one. Now you *could* do this by going through the index of a list of words and print the length of the words one at a time, taking up as many lines of code as you have indices. Needless to say, this is rather cumbersome. \n",
    "\n",
    "Python provides the so-called `for`-statements that allow us to iterate through an object and perform actions on its elements. Objects that allow iteration are called `iterables`. The built-in collections in Python are all iterables. The basic format of a `for`-statement is: \n",
    "\n",
    "    for ... in iterable:\n",
    "\n",
    "That reads almost like English. We can print all letters of the word *banana* as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3e5867a2",
   "metadata": {},
   "outputs": [],
   "source": [
    "for letter in \"banana\":\n",
    "    print(letter)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0ea1a548",
   "metadata": {},
   "source": [
    "The code in the loop is executed as many times as there are letters, with a different value for the variable `letter` at each iteration. Read the previous sentence again.\n",
    "\n",
    "Likewise we can print all the items that are contained in a list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "73edd8f1",
   "metadata": {},
   "outputs": [],
   "source": [
    "colors = [\"yellow\", \"red\", \"green\", \"blue\", \"purple\"]\n",
    "for whatever in colors:\n",
    "    print(\"This is color \" + whatever)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7ccbbbda",
   "metadata": {},
   "source": [
    "Since dictionaries are iterable objects as well, we can iterate through our book collection as well. This will iterate over the *keys* of a dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bd4c6c02",
   "metadata": {},
   "outputs": [],
   "source": [
    "for book in book_dict:\n",
    "    print(book)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e1c2281e",
   "metadata": {},
   "source": [
    "We can also iterate over both the keys and the values of a dictionary, this is done as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2c57b454",
   "metadata": {},
   "outputs": [],
   "source": [
    "for x, y in book_dict.items():\n",
    "    print(x + \" has score \" + str(y))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "77e1e986",
   "metadata": {},
   "source": [
    "Using `items()` will, at each iteration, return a nice pair of the key and the value. In the example above the variable `book` will loop over the keys of the dictionary, and the variable `score` loops over the respective values.\n",
    "\n",
    "The above way is the most elegant way of looping over dictionaries, but try to see if you understand the following alternative as well:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b4b90823",
   "metadata": {},
   "outputs": [],
   "source": [
    "for book in book_dict:\n",
    "    print(book, \"has score\", book_dict[book])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0f569ac3",
   "metadata": {},
   "source": [
    "-------"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "46c01275",
   "metadata": {},
   "source": [
    "### Recap"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "40246174",
   "metadata": {},
   "source": [
    "Here is an overview of the new concepts we have learnt in this section. Again, go through the list and make sure you understand them all.\n",
    "-  `for ... in ...`\n",
    "-  iterable objects\n",
    "-  variable assignment in a `for` loop"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41ddb457",
   "metadata": {},
   "source": [
    "--------"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8dfa9dea",
   "metadata": {},
   "source": [
    "<font color = royalblue>\n",
    "    \n",
    "### Exercise 6: word frequencies from a file\n",
    "    \n",
    "</font>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c8813cf0",
   "metadata": {},
   "source": [
    "The code in the box below loops through the lines in an iterable object that reads from a file. Collect word frequencies from the file by using split() on each line and iterating throug each word in an inner loop (loop inside the loop). Count each word separately in the dictionary (hint: use `.get()` to avoid KeyError)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b2f58688",
   "metadata": {},
   "outputs": [],
   "source": [
    "word_counts = {}\n",
    "with open(\"data/frankenstein.txt\", encoding = \"utf-8\") as file:\n",
    "    for line in file:\n",
    "        # your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ba76dc01",
   "metadata": {},
   "source": [
    "Let's see if you get the same result as we have:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "755e36a8",
   "metadata": {},
   "outputs": [],
   "source": [
    "word_counts[\"Frankenstein\"] == 9"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "095faded",
   "metadata": {},
   "source": [
    "Now, try improving the algorithm by stripping away punctuation on each word. You cound also try counting only capitalized words by using `if` and the string method `.isupper()` on the first character in each word."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "758747d6",
   "metadata": {},
   "source": [
    "<a rel=\"license\" href=\"http://creativecommons.org/licenses/by/4.0/\"><img alt=\"Creative Commons License\" style=\"border-width:0\" src=\"https://i.creativecommons.org/l/by/4.0/88x31.png\" /></a><br />This work is licensed under a <a rel=\"license\" href=\"http://creativecommons.org/licenses/by/4.0/\">Creative Commons Attribution 4.0 International License</a>."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
